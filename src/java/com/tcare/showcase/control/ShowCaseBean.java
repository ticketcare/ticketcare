/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  @version %I%, %G%
 */
package com.tcare.showcase.control;

import com.tcare.showcase.entities.Post;
import com.tcare.showcase.entities.PostFacade;
import com.tcare.util.web.JsfUtil;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;

/**
 * Managed bean for {@code /manage/showcase/post_create.xhtml}
 * @author hychen39@gmail.com
 */
@Named(value = "showCaseBean")
@ViewScoped
public class ShowCaseBean implements Serializable {

    private Post currentPost;
    @EJB
    private PostFacade postFacade;

    public Post getCurrentPost() {
        return currentPost;
    }

    @PostConstruct
    public void init(){
        currentPost = new Post();
        currentPost.setPostDate(JsfUtil.getCurrentDate());
    }
    
    // getters and setters
    public void setCurrentPost(Post currentPost) {    
        this.currentPost = currentPost;
    }

    /**
     * Creates a new instance of ShowCase
     */
    public ShowCaseBean() {
    }
    
    /** 
     *  Save the {@code currentPost} to database.
     * Post action: Reset the {@code currentPost} instance.
     */
    public void save(){
        postFacade.create(currentPost);
        currentPost = new Post();
    }
    
    public List<Post> findAllPosts(){
        List<Post> resultSet = null;
        resultSet = postFacade.findAll();
        return resultSet;
    }
    
    /**
     * Return posts of which the {@code give_date} is between the start and end dates.
     * if the {@code given_date} is null, return all posts.
     * @param given_date
     * @return 
     * @see PostFacade#findActivePosts(java.util.Date) 
     * 
     */
    public List<Post> findActivePosts(Date given_date){
        List<Post> resultSet;
        
        resultSet = (given_date != null)? postFacade.findActivePosts(given_date):
                postFacade.findAll();
            
        return resultSet;
    }
    
    
    
}
