/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  @version %I%, %G%
 */
package com.tcare.showcase.control;

import com.tcare.util.web.JsfUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * Validate the end date cannot be earlier than the start date. The UIComponent
 * for entering the start date is passed by the attribute with name
 * {@code startDateComponent}.
 *
 *
 * Reference:
 * {@link http://stackoverflow.com/questions/19093192/date-range-validation}
 *
 * @author hychen39@gmail.com
 */
@FacesValidator("dateRangeValidator")
public class DateRangeValidator implements Validator {

    public static final String START_DATE_COMPONENT = "startDateComponent";

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        // check the value of the end date
        if (value == null) {
            return;
        }

        // get the start date
        UIInput startDateComponent = (UIInput) component.getAttributes().get(START_DATE_COMPONENT);
        if (!startDateComponent.isValid()) {
            return;
        }

        Date startDate = (Date) startDateComponent.getValue();
        if (startDate == null) {
            return;
        }
        // get the end date
        Date endDate = (Date) value;

        // validate the end date
        if (startDate.after(endDate)) {
            ((UIInput)component).setValid(false);
            String clientId = component.getClientId();
            JsfUtil.addErrorMessage(clientId, clientId + 
                    "The end date cannot be before the start date.");
        }
    }

}
