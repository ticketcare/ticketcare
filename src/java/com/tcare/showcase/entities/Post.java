/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  @version %I%, %G%
 */
package com.tcare.showcase.entities;

import com.tcare.ticket.entities.Ticket;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 刊登 entity。
 * 一【票卷】可以有0次或多次的【刊登】。一個【刊登】只能對應到一【票卷】。
 * @author hychen39@gmail.com
 */
@Entity
@NamedQueries({
       @NamedQuery(name="findActivePosts", 
            query=" select p from Post p where p.startDate <= :given_date and p.endDate <= :given_date" )})
public class Post implements Serializable {
@TableGenerator(name = "POST_SEQ", table = "SEQ_TABLE", pkColumnValue = "POST")
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "POST_SEQ", strategy = GenerationType.TABLE)
    private Long id;

    // Reference to Ticket Instance
    @ManyToOne
    private Ticket ticket;
    
    @Temporal(TemporalType.DATE)
    @Column(name="START_DATE")
    private Date startDate;
    @Temporal(TemporalType.DATE)
    @Column(name="END_DATE")
    private Date endDate;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="POST_DATE")
    private Date postDate;

    private String description;
    @Basic(fetch = FetchType.LAZY)
    @Lob 
    @Column(name="FEATURE_IMG")
    private byte[] featureImg;
    
// getters and setters    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public byte[] getFeatureImg() {
        return featureImg;
    }

    public void setFeatureImg(byte[] featureImg) {
        this.featureImg = featureImg;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    

//--
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Post)) {
            return false;
        }
        Post other = (Post) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tcare.showcase.entities.Post[ id=" + id + " ]";
    }
    
}
