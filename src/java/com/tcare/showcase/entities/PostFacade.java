/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  @version %I%, %G%
 */
package com.tcare.showcase.entities;

import com.tcare.jpa_util.AbstractFacade;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 * The facade for {@link Post} entity.
 * @author hychen39@gmail.com
 */
@Stateless
public class PostFacade extends AbstractFacade<Post>  {

    @PersistenceContext(unitName = "TicketCareSystemPU")
    private EntityManager em;
    // constructor
    public PostFacade() {
        super(Post.class);
    }


    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
   
    /**
     * Return posts of which the {@code give_date} is between the start and end dates.
     * @param given_date
     * @return 
     */
    public List<Post> findActivePosts(Date given_date){
        List<Post> resultSet = null;
        TypedQuery<Post> query = em.createNamedQuery("findActivePosts", Post.class);
        query.setParameter("given_date", given_date);
        resultSet = query.getResultList();
        return resultSet;
    }
}
