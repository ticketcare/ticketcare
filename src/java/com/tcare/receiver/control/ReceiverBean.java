/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcare.receiver.control;

import com.tcare.receiver.entities.Receiver;
import com.tcare.receiver.entities.ReceiverDAO;
import com.tcare.receiver.entities.ReceiverFacade;
import javax.inject.Named;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * 票卷接受者
 *
 * @author hychen39@gmail.com
 */
@Named(value = "receiver")
@SessionScoped
public class ReceiverBean implements Serializable {

    static public final String FILENAME = "Receivers.txt";
    private int birthYear;
    private int birthMonth;
    private int birthDay;
    private Receiver receiverEntity;
    private ReceiverDAO receiverDAO;

//CDI
    @EJB
    private ReceiverFacade receiverFacade;

    /**
     * Creates a new instance of Receiver
     */
    public ReceiverBean() {
    }

    @PostConstruct
    public void initBean() {
        //defaul the birth date
        setBirthYear(2016);
        // Month index start from 0
        setBirthMonth(1);
        // Day index start from 1
        setBirthDay(1);
        // create a new Receiver entity instance
        receiverEntity = new Receiver();
        receiverEntity.setInterestings(new ArrayList<String>());
        // init ReceiverDAO
        receiverDAO = new ReceiverDAO();
        receiverDAO.setPath(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/"));
        receiverDAO.setFilename(FILENAME);
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public int getBirthMonth() {
        return birthMonth;
    }

    public void setBirthMonth(int birthMonth) {
        this.birthMonth = birthMonth;
    }

    public int getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(int birthDay) {
        this.birthDay = birthDay;
    }

    public Receiver getReceiverEntity() {
        return receiverEntity;
    }

    public void setReceiverEntity(Receiver receiverEntity) {
        this.receiverEntity = receiverEntity;
    }

    /**
     * Save form data to file.
     *
     * @return page to show the list of receivers.
     * 
     */
    public String save() {
        String targetPage = "/manage/cases/listAllReceivers";
        // create the year, month, and day fields to date
        StringBuilder birthDateBuilder = new StringBuilder();
        birthDateBuilder.append(getBirthYear()).append("/");
        birthDateBuilder.append(getBirthMonth()).append("/");
        birthDateBuilder.append(getBirthDay());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date birthDate = Calendar.getInstance().getTime();
        try {
            birthDate = sdf.parse(birthDateBuilder.toString());
        } catch (ParseException ex) {
            Logger.getLogger(ReceiverBean.class.getName()).log(Level.SEVERE, null, ex);
            // Throw error Message
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage("Errors occur when make the birth date."));
        }
        //Set the birth date property for the Receiver entity.
        receiverEntity.setBirth(birthDate);

        // call ReceiverDAO instance to save to file
        receiverDAO.save(receiverEntity);
        return targetPage;
    }
    
    public String saveDB(){
        String targetPage = "/manage/cases/listAllReceivers";
        // create the year, month, and day fields to date
        StringBuilder birthDateBuilder = new StringBuilder();
        birthDateBuilder.append(getBirthYear()).append("/");
        birthDateBuilder.append(getBirthMonth()).append("/");
        birthDateBuilder.append(getBirthDay());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date birthDate = Calendar.getInstance().getTime();
        try {
            birthDate = sdf.parse(birthDateBuilder.toString());
        } catch (ParseException ex) {
            Logger.getLogger(ReceiverBean.class.getName()).log(Level.SEVERE, null, ex);
            // Throw error Message
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage("Errors occur when make the birth date."));
        }
        //Set the birth date property for the Receiver entity.
        receiverEntity.setBirth(birthDate);

        // call ReceiverFacade to save to db.
        receiverFacade.create(receiverEntity);
        
        return targetPage;
    }

    /**
     * Get all receivers.
     *
     * @return
     */
    public List<Receiver> getReceivers() {
        List<Receiver> allList = null;
//        allList = receiverDAO.getAll();
        allList = receiverFacade.findAll();
        return allList;
    }

    /**
     * Create a new {@code Receiver} instance and assign to
     * {@link ReceiverBean#receiverEntity}.
     *
     * @return
     */
    public String addNew() {
        String targetPage = "/manage/cases/createCase";
        receiverEntity = new Receiver();
        setBirthYear(1980);
        setBirthMonth(0);
        setBirthDay(1);
        return targetPage;

    }
}
