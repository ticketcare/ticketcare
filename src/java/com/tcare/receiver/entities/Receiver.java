/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcare.receiver.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Receiver entity class
 * @author hychen39@gmail.com
 */
@Entity
public class Receiver implements Serializable{
    // constants
    static final public int MALE = 1;
    static final public int FEMAIL = 0;
    //id 
    @TableGenerator(name="RECEIVER_SEQ", table = "SEQ_TABLE", pkColumnValue = "RECEIVER")
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "RECEIVER_SEQ")
    private Long id;
    // fields
    private String name;
    private String idCardNumber;
    @Temporal(TemporalType.DATE)
    private Date birth;
    private int gender;
    private String phoneNumber;
    private String mobileNumber;
    private String address;
    @ElementCollection
    private List<String> interestings;
    private String note;
    
// default constructor
    public Receiver() {
    }
    // getters and setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdCardNumber() {
        return idCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        this.idCardNumber = idCardNumber;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<String> getInterestings() {
        return interestings;
    }

    public void setInterestings(List<String> interestings) {
        this.interestings = interestings;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    
}
