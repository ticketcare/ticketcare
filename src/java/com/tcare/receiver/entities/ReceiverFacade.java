/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  @version %I%, %G%
 */
package com.tcare.receiver.entities;

import com.tcare.jpa_util.AbstractFacade;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * 存取 Receiver table 的 EJB.
 * @author hychen39@gmail.com
 * @see AbstractFacade<T>
 */
@Stateless
@LocalBean
public class ReceiverFacade extends AbstractFacade<Receiver> {

    // CDI the entity manager
    @PersistenceContext(name = "TicketCareSystemPU")
    private EntityManager em;
    
    //default constructor
    
    public ReceiverFacade() {
        super(Receiver.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
