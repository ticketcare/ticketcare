/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcare.receiver.entities;

import com.tcare.receiver.entities.Receiver;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author hychen39@gmail.com
 */
public class ReceiverDAOHelper {

    static private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

    /**
     * Convert the fields in {@link Receiver} object to a string with CSV
     * format.
     *
     * Columns: (0)idCardNumber, (1)name, (2)gender, (3)birth, (4)phoneNumber,
     * (5) mobileNumber, (6) address, (7)interesting, (8)note
     *
     * @param receiver
     * @return
     */
    static public String getFromatStr(Receiver receiver) {
        String formatStr = null;
        StringBuilder sb = new StringBuilder();
        sb.append(receiver.getIdCardNumber()).append(", ");
        sb.append(receiver.getName()).append(", ");
        sb.append(receiver.getGender()).append(", ");

        String birthStr = sdf.format(receiver.getBirth());
        sb.append(birthStr).append(", ");
        sb.append(receiver.getPhoneNumber()).append(", ");
        sb.append(receiver.getMobileNumber()).append(", ");
        sb.append(receiver.getAddress()).append(", ");

        StringBuilder interestings = new StringBuilder();
        if (receiver.getInterestings() != null) {
            for (String i : receiver.getInterestings()) {
                interestings.append(i).append("-");
            }

        } else {
            interestings.append("null");
        }
        sb.append(interestings.toString()).append(", ");
        if (receiver.getNote() != null &&  !"".equals(receiver.getNote())) {
            sb.append(receiver.getNote());
        } else {
            sb.append("null");
        }

        formatStr = sb.toString();
        return formatStr;
    }

    /**
     * Parse the string generate from
     * {@link #getFromatStr(com.ticketCare.entities.Receiver)} to generate a
     * {@code Receiver} object
     *
     * @param fmtStr formated string
     * @return {@code Receiver} object
     */
    static public Receiver parseFormatStr(String fmtStr) {
        if (fmtStr == null) {
            return null;
        }

        Receiver receiver = new Receiver();

        String[] fields;
        fields = fmtStr.split(",\\s*");
        receiver.setIdCardNumber(fields[0]);
        receiver.setName(fields[1]);
        receiver.setGender(Integer.parseInt(fields[2]));
        try {
            //Date
            Date birthDate = sdf.parse(fields[3]);
            receiver.setBirth(birthDate);
        } catch (ParseException ex) {
            Logger.getLogger(ReceiverDAOHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        receiver.setPhoneNumber(fields[4]);
        receiver.setMobileNumber(fields[5]);
        receiver.setAddress(fields[6]);

        if (fields[7] != null) {
            String[] interistings = fields[7].split("-");
            receiver.setInterestings(Arrays.asList(interistings));
        } else {
            receiver.setInterestings(new ArrayList<String>());
        }

        receiver.setNote(fields[8]);

        return receiver;
    }
}
