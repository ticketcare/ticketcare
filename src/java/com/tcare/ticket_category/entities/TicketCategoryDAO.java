/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcare.ticket_category.entities;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;

/**
 * 存取 TicketEntity 的物件。
 *
 * @author hychen39@gmail.com
 */
public class TicketCategoryDAO {

    List<TicketCategory> allCategories = new ArrayList<>();

    /**
     * Get all categories.
     *
     * @return
     */
    public List<TicketCategory> getAllCategories() {

        allCategories.add(new TicketCategory("音樂"));
        allCategories.add(new TicketCategory("運動"));
        allCategories.add(new TicketCategory("展覽"));
        allCategories.add(new TicketCategory("音樂會"));

        return allCategories;
    }

    /**
     * Save the all ticket categories to file. The filename is
     * [Web_App_Root]/TicketCategory.txt
     *
     * @param newCategory
     */
    public void saveToFile(TicketCategory newCategory) throws Exception {
        // Update the cache 
        allCategories.add(newCategory);
        //Write to file
        // get the web application root
        String appPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
        // Open a file
        FileWriter fw = new FileWriter(appPath + "/TicketCategory.txt");
        // create a PrintWriter with auto-flush feature.
        PrintWriter pw = new PrintWriter(fw, true); 
            for (TicketCategory category : allCategories) {
                pw.println(category.getName());
            }
            // close file
            pw.close();
    }
    
    /**
     * Search the ticket categories that meet the giving term.
     * @param term
     * @return 
     */
    public List<TicketCategory> searchCategory(String term){
        List<TicketCategory> results = new ArrayList<>();
        for(TicketCategory c: allCategories){
            if (c.getName().contains(term)){
                results.add(c);
            }
        }
        
        return results;
    }
    
    /**
     * Edit the name of the given {@link TicketCategory} object.
     * @param category existing {@link TicketCategory} object.
     * @param newName new category name
     */
    public void editCategory(TicketCategory category, String newName){
        category.setName(newName);
    }
    /**
     * 檢查給予的票卷類別名稱是否已存在。
     * @param category
     * @return 
     */
    public boolean isContain(TicketCategory category){
        boolean isExist = false;
        isExist = allCategories.contains(category);
        return isExist;
    }
}
