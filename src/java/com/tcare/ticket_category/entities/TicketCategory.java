/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcare.ticket_category.entities;

import com.tcare.util.web.SelectItemLabelValue;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

/**
 * Ticket category entity.
 * @author user
 */
@Entity
@Table(name = "TICKET_CAT")
public class TicketCategory implements Serializable {
    @TableGenerator(name="TICKET_CATEGORY_SEQ", table = "SEQ_TABLE", pkColumnValue = "TICKET_CATEGORY")
    @Id @GeneratedValue(strategy = GenerationType.TABLE, generator = "TICKET_CATEGORY_SEQ")
    @Column(name = "ID")
    private Long id;
    
    @Column(name = "NAME")
    private String name;

    @Column(name="DESCRIPTION")
    private String desc;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public TicketCategory(String name) {
        this.name = name;
    }

    public TicketCategory() {
        this.name="";
        this.desc="";
    }

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TicketCategory other = (TicketCategory) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }

    
    
}
