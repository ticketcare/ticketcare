/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  @version %I%, %G%
 */
package com.tcare.ticket_category.entities;

import com.tcare.jpa_util.AbstractFacade;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author hychen39@gmail.com
 */
@Stateless
@LocalBean
public class TicketCategoryFacade extends AbstractFacade<TicketCategory>{

    @PersistenceContext(name = "TicketCareSystemPU")
    private EntityManager em;
    
    public TicketCategoryFacade() {
        super(TicketCategory.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
