/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  @version %I%, %G%
 */
package com.tcare.ticket_category.control;

import com.tcare.ticket_category.entities.TicketCategory;
import com.tcare.ticket_category.entities.TicketCategoryFacade;
import javax.ejb.EJB;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Convert {@link TicketCategory} ID from or to {@link TicketCategory} Object.
 *
 * @author hychen39@gmail.com
 */
@FacesConverter("ticketCategoryConverter")
public class TicketCategoryConverter implements Converter {

    @EJB
    private TicketCategoryFacade categoryFacade;

    private TicketCategory currentTicketCategory;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        try {
            Long pkValue = Long.parseLong(value);
            currentTicketCategory = categoryFacade.find(pkValue);
        } catch (NumberFormatException exception) {
            return null;
        }
        return currentTicketCategory;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {

        try {
            currentTicketCategory = (TicketCategory) value;
        } catch (ClassCastException exception) {
            return null;
        }
        // return the object ID.
        return currentTicketCategory.getId().toString();
    }

}
