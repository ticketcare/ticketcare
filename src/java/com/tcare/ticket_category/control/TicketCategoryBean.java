/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcare.ticket_category.control;

import com.tcare.ticket_category.entities.TicketCategory;
import com.tcare.ticket_category.entities.TicketCategoryDAO;
import com.tcare.ticket_category.entities.TicketCategoryFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

/**
 *
 * @author user
 */
@Named(value = "ticketCategoryBean")
@SessionScoped
public class TicketCategoryBean implements Serializable {
    
    @EJB
    private TicketCategoryFacade ticketCategoryFacade;
    
    private List<TicketCategory> categoryList;
    private TicketCategoryDAO ticketCategoryDAO = new TicketCategoryDAO();
    private String ticketCategoryName;
    private String description;
    private TicketCategory currentCategory;
    
    /**
     * Search term inputted in the search box.
     */
    private String searchTerm;

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public TicketCategory getCurrentCategory() {
        return currentCategory;
    }

    public void setCurrentCategory(TicketCategory currentCategory) {
        this.currentCategory = currentCategory;
    }

    
    
    public String getTicketCategoryName() {
        return ticketCategoryName;
    }

    public void setTicketCategoryName(String ticketCategoryName) {
        this.ticketCategoryName = ticketCategoryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    
       
    /**
     * Creates a new instance of TicketCategoryBean
     */
    public TicketCategoryBean() {
//       categoryList = ticketCategoryDAO.getAllCategories();
       
    }

    /**
     * Init the Managed bean
     */
    @PostConstruct
    public void init(){
       categoryList = ticketCategoryFacade.findAll(); 
    }
    
    public List<TicketCategory> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<TicketCategory> categoryList) {
        this.categoryList = categoryList;
    }
    
    /**
     * 將輸入的新的票卷類別存到後端。
     * @return Navigate to page {@code /ticketCategory/ticketCategory}
     * @throws java.lang.Exception
     * @deprecated 
     */
    public String saveCategory() throws Exception{
        TicketCategory newCategory = new TicketCategory(ticketCategoryName);
        ticketCategoryDAO.saveToFile(newCategory);
        return  "/manage/ticketCategory/ticketCategory";
    }
    /**
     * Save the TicketCategory to DB.
     * @return
     * @deprecated 
     */
    public String saveDB(){
        TicketCategory newCategory = new TicketCategory();
        newCategory.setName(this.ticketCategoryName);
        newCategory.setDesc(this.description);
        ticketCategoryFacade.create(newCategory);
        this.refreshList();
        return  "/manage/ticketCategory/ticketCategory";
    }
    
    /**
     * Save {@link #currentCategory} to database.
     */
    public void savdDB(){
        ticketCategoryFacade.edit(currentCategory);
        this.refreshList();
    }
  
    /**
     * Remove the {@link #currentCategory} from the database
     */
    public void remove(){
        ticketCategoryFacade.remove(currentCategory);
        this.refreshList();
    }
    /**
     * Get all records from db and store to {@code categoryList} field.
     */
    public void refreshList(){
        categoryList = ticketCategoryFacade.findAll();
    }
    /**
     * 依據 {@code searchTerm} property 內的值查詢符合資格的類別名稱。
     * @return Return page string. 
     */
    public String searchCategory(){
        categoryList = ticketCategoryDAO.searchCategory(searchTerm);
        return null;
    }
    
    /**
     * Action to edit an existing category.
     * @param category
     * @return return page string
     */
    public String editCategory(TicketCategory category){
        
        return null;
    }
    
    /**
     * User defined validation method for 
     * {@link #editCategory(com.ticketCare.ticketCategory.entities.TicketCategory) } property.
     * 
     * @param context
     * @param component
     * @param value
     */
   public void validateCategoryName(FacesContext context, UIComponent component, Object value){
       String inputNewValue;
       inputNewValue = (String) value;
       UIInput inputBox = (UIInput) component;
       String msg1 = "欄位不能空白";
       if ("".equals(inputNewValue) || inputNewValue == null){
           // validation fail
           inputBox.setValid(false);
           context.addMessage(inputBox.getClientId(), new FacesMessage(msg1));
           
       }
       // check if the name is duplicated.
       // 屬於後端驗證
       TicketCategory newTicketCategory = new TicketCategory(inputNewValue);
       
       boolean isContain = ticketCategoryDAO.isContain(newTicketCategory);
       
       if (isContain){
           String msg2 = "名稱不能重複";
           inputBox.setValid(false);
           context.addMessage(inputBox.getClientId(), new FacesMessage(msg2));
       }
   }
   
   /**
    * Create a new instance for {@link #currentCategory} reference variable.
    * @param event 
    */
   public void prepareNew(ActionEvent event){
       currentCategory = new TicketCategory();
   }
   
   
}
