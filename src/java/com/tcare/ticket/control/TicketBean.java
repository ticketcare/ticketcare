/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  @version %I%, %G%
 */
package com.tcare.ticket.control;

import com.tcare.ticket.entities.Ticket;
import com.tcare.ticket.entities.TicketFacade;
import com.tcare.ticket_category.entities.TicketCategory;
import com.tcare.ticket_category.entities.TicketCategoryFacade;
import com.tcare.util.web.JsfUtil;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.model.SelectItem;

/**
 * 處理 Ticket 輸入UI 的 managed bean. Facelet page:
 * <ul>
 * <li> /manage/tickets/ticket_create.xhtml </li>
 * <li> /manage/showcase/ticketlist.xhtml </li>
 * </ul>
 *
 * @author hychen39@gmail.com
 */
@Named(value = "ticketBean")
@SessionScoped
public class TicketBean implements Serializable {

    /**
     * 按下 save() 後的轉跳頁面。
     */
    public static final String REVIEW_PAGE = "/manage/tickets/ticket_review";
    public static final String CREATE_PAGE = "/manage/tickets/ticket_create";
    //Inject the required JPA controller
    @EJB
    private TicketFacade ticketFacade;
    @EJB
    private TicketCategoryFacade ticketCategoryFacade;

    // Ticket entity to store the inputted data.
    private Ticket currenTicket;

    /**
     * Creates a new instance of TicketBean
     */
    public TicketBean() {
    }

    //postconstuct
    @PostConstruct
    private void init() {
        currenTicket = new Ticket();
        currenTicket.setCreate_date(getCurrentDate());
    }

    // Getters and Setters
    public Ticket getCurrenTicket() {
        return currenTicket;
    }

    public void setCurrenTicket(Ticket currenTicket) {
        this.currenTicket = currenTicket;
    }

    private Date getCurrentDate() {
//        Calendar gCal = Calendar.getInstance();
//        return gCal.getTime();
        return JsfUtil.getCurrentDate();
    }

       
    public List<TicketCategory> getTicketCategoryObjects(){
        return ticketCategoryFacade.findAll();
    }

    public String save() {
        ticketFacade.edit(currenTicket);
        prepareCreate();
        return CREATE_PAGE;
    }

    public String prepareCreate() {
        //prepare next input
        currenTicket = new Ticket();
        currenTicket.setCreate_date(getCurrentDate());
        return CREATE_PAGE;
    }

    /**
     * Get all tickets from the database.
     *
     * @return
     */
    public List<Ticket> getTickets() {
        return ticketFacade.findAll();
    }
}
