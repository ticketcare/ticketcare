/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *  @version %I%, %G%
 */
package com.tcare.ticket.control;

import com.tcare.ticket.entities.Ticket;
import com.tcare.ticket.entities.TicketFacade;
import javax.ejb.EJB;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 * Ticket Converter 
 * @author hychen39@gmail.com
 */
@FacesConverter("ticketConverter")
public class TicketConverter implements Converter {

    @EJB
    TicketFacade ticketFacade;
    /**
     * Convert value to object. 
     * @param context
     * @param component
     * @param value Id for the {@code Ticket} object.
     * @return 
     * ToDo: Test
     */
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        Ticket ticket=null;
        Long pkValue = new Long(value);
        ticket = ticketFacade.find(pkValue);
        return ticket;
    }

    /**
     * Convert object to String value to appear on the page.
     * @param context
     * @param component
     * @param value
     * @return 
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        Ticket ticket = (Ticket) value;
        return ticket.getId().toString();
    }
    
}
