function handleSubmit(args, dialog) {
    var jqDialog = jQuery('#' + dialog);
    if (args.validationFailed) {
        jqDialog.effect('shake', {times: 3}, 100);
//        jqDialog.effect('bounce', {times: 4, distance: 10}, 500);
    } else {
        PF(dialog).hide();
    }
}
