/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcare.entities;

import com.tcare.receiver.entities.ReceiverDAO;
import com.tcare.receiver.entities.Receiver;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hychen39@gmail.com
 */
public class ReceiverDAOTest {

    List<Receiver> receivers;

    public ReceiverDAOTest() {
        receivers = new ArrayList<>();
        Receiver temp = new Receiver();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        
        temp.setName("Cat Puzzle");
        try {
            temp.setBirth(sdf.parse("1973/08/10"));
        } catch (ParseException ex) {
            Logger.getLogger(ReceiverDAOTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        temp.setNote("Record1");
        receivers.add(temp);
        
        temp = null;
        temp = new Receiver();
        temp.setName("Dog Dog");
        try {
            temp.setBirth(sdf.parse("1980/09/30"));
        } catch (ParseException ex) {
            Logger.getLogger(ReceiverDAOTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        temp.setNote("Record2");
        receivers.add(temp);
        
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getAll method, of class ReceiverDAO.
     */
    @Test
    public void testGetAll() {
        System.out.println("test getAll...");
        // make sure write data to file.
        testWrite();
        // Test starts from here
        ReceiverDAO instance = new ReceiverDAO();
        instance.setFilename("Receives.txt");
        String currentPath =System.getProperty("user.dir"); 
        instance.setPath(currentPath);
        System.out.println(currentPath);

        List<Receiver> result = instance.getAll();
        assertEquals(2, result.size());
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of write method, of class ReceiverDAO.
     */
    @Test
    public void testWrite() {
        System.out.println("test write...");
        ReceiverDAO instance = new ReceiverDAO();
        instance.setFilename("Receives.txt");
        String currentPath =System.getProperty("user.dir"); 
        instance.setPath(currentPath);
        System.out.println(currentPath);
        instance.write(receivers);
        assertTrue(true);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test the scenario that the file does not exist when loading data.
     */
    @Test
    public void testLoadFromFile() {
        System.out.println("test loadFromFile...");
        // Test starts from here
        ReceiverDAO instance = new ReceiverDAO();
        instance.setFilename("Receives-notExist.txt");
        String currentPath =System.getProperty("user.dir"); 
        instance.setPath(currentPath);
        System.out.println(currentPath);

        List<Receiver> result = instance.getAll();
        assertEquals(0, result.size());
        // TODO review the generated test code and remove the default call to fail.
    }
}
