/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcare.entities;

import com.tcare.receiver.entities.ReceiverDAOHelper;
import com.tcare.receiver.entities.Receiver;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hychen39@gmail.com
 */
public class ReceiverDAOHelperTest {
    
    public ReceiverDAOHelperTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getFromatStr method, of class ReceiverDAOHelper.
     */
    

    /**
     * Test of parseFormatStr method, of class ReceiverDAOHelper.
     */
    @Test
    public void testParseFormatStr() throws ParseException {
        System.out.println("parseFormatStr");
        String fmtStr = ", Test 4, 0, 1980/01/01, , , , , null";
        Receiver expResult = null;
        Receiver result = ReceiverDAOHelper.parseFormatStr(fmtStr);
        Date birthDate = new SimpleDateFormat("yyyy/MM/dd").parse("1980/01/01");
        assertEquals(birthDate, result.getBirth());
        // TODO review the generated test code and remove the default call to fail.
    }
    
}
