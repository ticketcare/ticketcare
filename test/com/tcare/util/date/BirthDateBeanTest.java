/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tcare.util.date;

import com.tcare.util.date.BirthDateBean;
import java.util.Calendar;
import java.util.List;
import javax.faces.model.SelectItem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author hychen39@gmail.com
 */
public class BirthDateBeanTest {

    BirthDateBean birthDateBean;

    public BirthDateBeanTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        birthDateBean = new BirthDateBean();
        birthDateBean.init();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of init method, of class BirthDateBean.
     */
    @Test
    public void testgetMonthItems() {
        System.out.println("test getMonthItems..");

        // TODO review the generated test code and remove the default call to fail.
        List<SelectItem> monthsItems = birthDateBean.getMonthItems();
        assertEquals(monthsItems.size(), 12);
        System.out.println(monthsItems.get(0).getLabel());
        assertEquals(monthsItems.get(0).getValue(), 0);
        System.out.println(monthsItems.get(1).getLabel());
        assertEquals(monthsItems.get(1).getValue(), 1);
        System.out.println(monthsItems.get(6).getLabel());
    }

    /**
     * Test of getDayItems method, of class BirthDateBean.
     */
    @Test
    public void testGetDayItems() {
        System.out.println("test getDayItems..");
        int year = 2016;
        int monthCode = Calendar.FEBRUARY;

        List<SelectItem> result = birthDateBean.getDayItems(year, monthCode);
        assertEquals(29, result.size() );
        assertEquals("1", result.get(0).getLabel());
        // TODO review the generated test code and remove the default call to fail.
    }

  
    /**
     * Test of getCurrentYear method, of class BirthDateBean.
     */
    @Test
    public void testGetCurrentYear() {
        System.out.println("test getCurrentYear...");
        int expResult = 2016;
        int result = birthDateBean.getCurrentYear();
        assertEquals(expResult, result);
    }

}
